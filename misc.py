"""
some miscilanious functions
"""
import numpy as np 
import matplotlib.pyplot as plt 
from matplotlib import gridspec 
import os 
import pdb 
import scipy.interpolate as interp 
import scipy.stats as stats 
from scipy.optimize import curve_fit 
import scipy.stats 
import inspect 

class plotdiff:
	def __init__(self):
		plt.figure(figsize=(8,6))
		self.gs = gridspec.GridSpec(2, 1, height_ratios=[2, 1])
		#plt.subplot(self.gs[0])
		#plt.plot(x,y,**kwargs)
		#plt.subplot(self.gs[1])
		#plt.plot(x,y-y0,**kwargs) 

	def plotdiff(self,x,y,y0,error=None, ylabel='', ylabel_diff = '', fontsize = 18, fmt='.', fontsize2=16, **kwargs):
		ax = plt.subplot(self.gs[0])
		if error is None:
			plt.plot(x,y,**kwargs)
		else:
			plt.errorbar(x,y,yerr=error,fmt=fmt,**kwargs)
		if ylabel != '':
			plt.ylabel(ylabel,fontsize = fontsize)
		ax1 = plt.subplot(self.gs[1])
		if error is None:
		    plt.plot(x,y-y0,**kwargs) 
		else:
			plt.errorbar(x,y-y0,yerr=error,fmt=fmt,**kwargs)
		if ylabel_diff != '':
			plt.ylabel(ylabel_diff, fontsize = fontsize)

		plt.setp(ax.get_xticklabels(), fontsize=fontsize2)
		plt.setp(ax1.get_xticklabels(), fontsize=fontsize2)
		plt.setp(ax.get_yticklabels(), fontsize=fontsize2)
		plt.setp(ax1.get_yticklabels(), fontsize=fontsize2)
		plt.yticks(np.arange(-400, 400+1, 200))

	def xlabel(self, label, gs=0, **kwargs):
		plt.subplot(self.gs[gs])
		plt.xlabel(label, **kwargs)

	def xlim(self,lim):
		plt.subplot(self.gs[0])
		plt.xlim(lim)
		plt.subplot(self.gs[1])
		plt.xlim(lim)		

	def legend(self):
		plt.subplot(self.gs[0])
		plt.legend()

	def addplotlabel(self,label='(a)',fontsize=18):
		plt.subplot(self.gs[0])
		plt.text(0-(1500)*0.17,6000-(6000)*0.05,label,fontsize=fontsize)


def bin_array(x,nbins=10,equal=False, unique=False):
	minx = x.min()
	maxx = x.max()

	if equal == True:
		x_u = np.unique(x)
		sortedx = np.sort(x_u)
		n = int(len(x_u)/float(nbins))
		loc = np.arange(0,len(x_u)+1,n)
		loc[-1] = len(x_u)-1
		assert len(loc) == nbins+1
		binedges = sortedx[loc]
	else:
		binedges = np.linspace(minx,maxx,nbins+1)
	binedges[-1] = binedges[-1]+0.001
	out = []
	for i in xrange(nbins):
		out.append((x >= binedges[i])*(x < binedges[i+1]))
	return np.array(out), binedges


def scatter2binned_1d(x_in,y_in,nbins=8,weights=None,equal=False,emptyerror=True,unique=False, jk=None, comm=None, trim=None):
	"""
	converts scatter points to a set of x, y points with errors
	returns x,y,err
	weights now work (hmmm.... check the err.append line before using for anything important)
	"""
	if jk is not None:
		raise RuntimeError('Dont use this function for jackknife. It is garbage')
		assert len(jk) == len(x_in) #not sure if right
	if trim is not None:
		#a good value for trim might be 0.001
		if isinstance(trim,float) == True:
			h = np.histogram(x_in, bins = 100)
			t = trim*h[0].max()
			bulk = np.where(h[0] > t)[0]
			mi = h[1][bulk[0]]
			mx = h[1][bulk[-1]+1]
		else:
			mi,mx = trim
		print 'cutting x,y,err between {0} and {1}'.format(mi,mx)
		select = (x_in >= mi)*(x_in <= mx)
		print 'cut removes {0} of points'.format(sum((~select).astype('int'))/float(len(select)))
		x_in = x_in[select]
		y_in = y_in[select]
		if weights is not None:
			weight = weights[select]
		if jk is not None:
			jk = jk[select]


	if jk is not None:
		assert equal == False
	if weights is None:
		weights = np.ones(len(x_in))
	bins,edges = bin_array(x_in,nbins=nbins,equal=equal,unique=unique)

	x,y,err = make_1d_arrays(x_in, y_in, bins, weights, emptyerror)

	if jk is not None:
		cov = jackknife_cov(x_in, y_in, bins, jk, weights,comm=comm)
		err = np.sqrt(np.diagonal(cov))
	return x,y,err


def jackknife_cov(x_in, y_in, bins, jk, weights,comm=None):

	if comm is not None:
		from mpi4py import MPI 
		rank = comm.Get_rank()
		size = comm.Get_size()
		if size > 1:
			raise IOError('jackknife not setup for mpi yet')

	region_list = np.unique(jk)
	dvecs = []
	for index, region in enumerate(region_list):
		x_jk = x_in[jk != region]
		y_jk = y_in[jk != region]
		weights_jk = weights[jk != region]
		bins_jk = bins[:,jk != region]

		#pdb.set_trace()

		x,y,err = make_1d_arrays(x_jk, y_jk, bins_jk, weights_jk, emptyerror=False)

		dvecs.append(y)
	dvecs = np.array(dvecs)
	cov_mat = covariance(dvecs)

	return cov_mat


def make_1d_arrays(x_in, y_in, bins, weights, emptyerror):
	print 'make 1d arrays'
	x = []
	y = []
	err = []
	for bin in bins:
		if sum(bin.astype('int')) == 0:
			print 'empty bin'
			if emptyerror == True:
				raise RuntimeError('empty bin in misc function')
			else:
				continue
		x.append(np.average(x_in[bin], weights=weights[bin]))
		y.append(np.average(y_in[bin], weights=weights[bin]))
		err.append(std(y_in[bin],weights=weights[bin])/np.sqrt(sum(bin.astype('int'))))
	x = np.array(x)
	y = np.array(y)
	err = np.array(err)
	return x,y,err

def covariance(dvec_list, v=True):
	"""
	produces a covariance matrix from an array of data vectors (e.g. many mocks)
	same as np.cov(dvec_list,rowvar=0)
	"""
	if isinstance(dvec_list,list) == True:
		dvec_list = np.array(dvec_list)

	nmocks = len(dvec_list)
	dvec_len = len(dvec_list[0])

	if v == True:
		print 'nmocks {0}'.format(nmocks)
		print 'dvec_len {0}'.format(dvec_len)

	cov_mat = np.zeros((dvec_len,dvec_len))
	for dvec_i in xrange(dvec_len):
		for dvec_j in xrange(dvec_len):
			x = dvec_list[:,dvec_i]
			y = dvec_list[:,dvec_j]
			cov_mat[dvec_i,dvec_j] = np.mean((x-np.mean(x))*(y-np.mean(y)))

	return cov_mat


def interp_extrap_1d(x,y,x_val,kind='linear',n4lin=2):
	y_out = interp.interp1d(x,y,kind=kind,bounds_error=False)(x_val) #do usual 1d interpolation(numbers outside of x range will be nans)
	
	#get any extrapolated values from polyfit
	if kind=='linear':
		polyorder = 1
	else:
		polyorder = 1 #this feature is not added yet
	
	#high points
	select = (np.isnan(y_out))*(x_val >= x.max())
	if sum(select.astype('int')) > 0:
		s = np.argsort(x)
		y_out[select] = np.polyval(np.polyfit(x[s][-1*n4lin:],y[s][-1*n4lin:],polyorder),x_val[select])

	#low points
	select = (np.isnan(y_out))*(x_val <= x.min())
	if sum(select.astype('int')) > 0:
		s = np.argsort(x)
		y_out[select] = np.polyval(np.polyfit(x[s][:n4lin],y[s][:n4lin],polyorder),x_val[select])

	return y_out

def interp_extrap_nd(x,y,x_val,method='linear'):
	"""
	Will interpolated in nd using griddata, numbers outside of range will be given nearest value
	"""	
	y_out = interp.griddata(x,y,x_val,method=method)
	
	y_out[np.isnan(y_out)] = interp.griddata(x,y,v_val[np.isnan(y_out)],method='nearest')
	print 'WARNING: this function is using "nearest" for the out of bounds input'

	if np.isnan(y_out).any() == True:
		print 'WARNING: there are some nans'

	return y_out

def lowest_poly(x_in,y_in,nbins=10,equal=False,maxorder=10,reportnonlin=False,startat0=False):
	"""
	finds lowest order polynomial that has a good chi2 fit
	takes a set of scatter points as input (assumes errors are equal on all points)
	bins these points, the fits curve to binned points
	will only calculate polyfits up to maxorder
	returns coeff
	"""
	import scipy.stats

	x,y,err = scatter2binned_1d(x_in,y_in,nbins=nbins,equal=equal)
	if (np.isnan(err)).any() == True:
		x = x[np.isnan(err)==False]
		y = y[np.isnan(err)==False]
		err = err[np.isnan(err)==False]
	if (np.isnan(y)).any() == True:
		x = x[np.isnan(y)==False]
		err = err[np.isnan(y)==False]
		y = y[np.isnan(y)==False]

	if startat0 == True:
		firstpoly = 0
	else:
		firstpoly=1
	orderlist = np.arange(firstpoly,maxorder)
	for polyorder in orderlist:
		coeff = np.polyfit(x,y,polyorder,w=1./err)
		y_pred = np.polyval(coeff,x)

		#calc chi2
		chi2 = sum(((y-y_pred)**2.)/err)
		print '{1} chi2 {0}'.format(chi2,polyorder)

		if chi2 <= 3.:
			lowest_order = polyorder
			break

	print 'Lowest order {0}'.format(lowest_order)
	if reportnonlin == True:
		if lowest_order > 1:
			print '!!!!!!!!!!! Non-linear fit here !!!!!!!!!!!!!!!!'
	return coeff


def plotlog(x,y,color='blue',label = '',**kwargs):
	"""
	makes loglog plots with negative values dotted lines
	"""

	ypos = np.where(y >= 0, y, np.nan)
	plt.loglog(x, ypos, marker='o', linestyle='-', color = color, label = label, **kwargs)

	if (y < 0).any() == True:
		yneg = np.where(y < 0, y, np.nan)
		plt.loglog(x, abs(yneg), marker='o', linestyle='--', color=color, **kwargs)

	return

def makenz(zp,zp_err,save = False, filename = './nz.txt',returnnz=True,bins=200):
	assert len(zp) == len(zp_err)
	assert (save == True) or (returnnz == True)
	nz_z = np.linspace(0.,2.,bins)
	nobjects = len(zp)
	nz=np.zeros(len(nz_z))
	for i in xrange(nobjects):
		#print("{0}/{1}".format(i,nobjects))
		pz = stats.norm(loc = zp[i], scale=zp_err[i]).pdf(nz_z)
		nz = nz + pz

	if save == True:
		np.savetxt(filename, np.transpose([nz_z,nz]))

	if returnnz == True:
		return nz_z, nz
	else:
		return


def arctan_interp((x,y),z,(x_vals,y_vals)):
	"""
	for lasagna thermalisation surfaces
	"""

	def func((xin,yin),A,B,C,D,E):
		r = D*xin + E*yin
		out = A*np.arctan(B*r)+C
		out[out > 1] = 1.
		out[out < 0] = 0
		return out

	p = curve_fit(func,(x,y),z)

	zout = func((x_vals,y_vals),*p[0])

	return zout

line_style_list = ['-','--','-.',':']
class line_style:
	def __init__(self):
		self.num = 0

	def next(self):
		old = np.copy(self.num)
		if self.num == len(line_style_list)-1:
			self.num = 0
		else:
			self.num += 1
		return  line_style_list[old]
master_style = line_style()
def nextstyle():
	return master_style.next()

dash_style_list = [ [100000,1],
					[8, 4, 2, 4, 2, 4],
					[10,10],
					[30,2,2,2,2,2,2,2,2,2,2,2],
					[1,1]]
class dash_style:
	def __init__(self):
		self.num = 0

	def next(self):
		old = np.copy(self.num)
		if self.num == len(dash_style_list)-1:
			self.num = 0
		else:
			self.num += 1
		return  dash_style_list[old]
master_dash = dash_style()
def nextdash():
	return master_dash.next()

color_list = ['b','r','c','y','k','m','g']
class color:
	def __init__(self):
		self.num = 0

	def next(self):
		old = np.copy(self.num)
		if self.num == len(color_list)-1:
			self.num = 0
		else:
			self.num += 1
		return  color_list[old]
master_color = color()
def nextcolor():
	return master_color.next()

def std(a, weights=None):
	if weights is not None:
		weights = np.ones(len(a))

	average = np.average(a, weights=weights)
	variance = np.average((a-average)**2, weights=weights)  # Fast and numerically precise
	return np.sqrt(variance)

def chi2(cl, k):
	"""
	cl == Confidance level (1sigma -> cl 68.3%)
	k  == dof
	"""
	return scipy.stats.chi2.ppf(cl, k)

def cosmosis_theory_plots(filedir, savedir = None, thetamin=0.5, thetamax = 5., skipexisting=False):
	assert os.path.exists(filedir)
	if filedir[-1] != '/':
		filedir = filedir + '/'
	if savedir is None:
		savedir = filedir + 'plots/'
	if os.path.exists(savedir) == False:
		os.mkdir(savedir)

	dirlist = os.listdir(filedir)
	corr_names = ['shear_xi', 'galaxy_xi', 'galaxy_shear_xi']
	binnames = {'shear_xi':['ximinus', 'xiplus'], 'galaxy_xi':['bin'], 'galaxy_shear_xi':['bin']}

	for corr_name in corr_names:
		theta = np.loadtxt(filedir+ corr_name + '/theta.txt')
		theta = theta*180. / np.pi
		theta_select = (theta > thetamin)*(theta < thetamax)
		for binname in binnames[corr_name]:
			filelist = os.listdir(filedir + corr_name)
			filelist = [x for x in filelist if binname in x]
			for filename in filelist:
				bin1 = filename.split('_')[-2]
				bin2 = filename.split('_')[-1].split('.')[0]
				label = corr_name + '_' + binname + '_' + '{0}_{1}'.format(bin1, bin2)
				if skipexisting == True and os.path.exists(savedir + label+'.png'):
					print 'skipping ' + savedir + label+'.png'
					continue
				data = np.loadtxt(filedir + corr_name + '/' + filename)
				plt.plot(theta[theta_select], data[theta_select], label=label)
				plt.xlabel(r'$\theta$', fontsize = 18)
				plt.tight_layout()
				plt.savefig(savedir + label+'.png')
				print 'saved ' + savedir + label+'.png'
				plt.close()

	return


def cov_index(cov_mat, bin, nthetabins, cross=True):
	if cross == True:
		start = nthetabins*(sum(5-np.arange(bin)))
	else:
		raise IOError('cross==False option not yet coded')
	finish = start + nthetabins
	return cov_mat[start:finish, start:finish]



def errorbar(x,y,err,label=''):

	ax = plt.gca()
	if len(ax.get_lines()) != 0:
		x_old = ax.get_lines()[0].get_xdata()
		if (errorbar_x == x).all() == True:
			dx = (x[1:]-x[:-1])/10.
			dx = np.append(dx, dx[-1])
			x_new = x + dx
		else:
			x_new = x
	else:
		x_new = x
	plt.errorbar(x_new,y,err,fmt='.',label=label)

	return

def calc_chi2(y, err, yfit , v = True):
	if err.shape == (len(y),len(y)):
		#use full covariance
		if v == True:
			print 'cov_mat chi2'
		inv_cov = np.linalg.inv( np.matrix(err) )
		chi2 = 0
		for i in xrange(len(y)):
			for j in xrange(len(y)):
				chi2 = chi2 + (y[i]-yfit[i])*inv_cov[i,j]*(y[j]-yfit[j])
		return chi2
		
	elif err.shape == (len(y),):
		print 'diagonal chi2'
		return sum(((y-yfit)**2.)/(err**2.))
	else:
		raise IOError('error in err or cov_mat input shape')

def minimize_chi2(func, x,y,err, tol = 10000, stepsize = 0.1, returnchain=False,ommit_exception=True):
	"""
	tol = number of steps that can pass without improvement before the chain stops
	ommit_exception now does nothing
	"""
	nparams = len(inspect.getargspec(func)[0][1:])
	if err.shape == (len(y),len(y)):
		err1 = np.sqrt(np.diagonal(err)) #just for the initial estimate
	elif err.shape == (len(y),):
		err1 = np.copy(err)
	else:
		raise IOError('error not in err or cov_mat input shape')

	#find best fit with independant errors
	#p0,cov = scipy.optimize.curve_fit(func, x, y, sigma = err1)
	try:
		p0,cov = scipy.optimize.curve_fit(func, x, y, sigma=err1)
	except RuntimeError:
		print 'Runtime error in p0,cov, starting minimize at null test value'
		tol = tol*2
		if nparams == 2:
			p0 = np.array([0,np.mean(y)])
		elif nparams == 3:
			p0 = np.array([np.mean(y)/2., max(x), 0.5])
		cov = np.inf*np.ones((nparams,nparams))

	chi2 = calc_chi2(y, err, func(x,*p0), v=False)

	def step(cov1):
		# will produce a proposed step in teh parameter space
		# step in 1d is drawn from gaussian with mean =0, std = stepsize * err
		# where err is the error in the parameter assuming the points are independent
		l = len(cov1)
		s = np.zeros(l)
		for i in xrange(l):
			if cov[i,i] == np.inf:
				cov[i,i] = p0[i]*0.05
			s[i] = np.random.normal(loc=0,scale=stepsize*np.sqrt(cov1[i,i]))
		return s

	count = 0
	chi2_list = []
	p0_list = []
	while count < tol:
		p_tmp = p0 + step(cov)
		chi2_tmp = calc_chi2(y,err,func(x, *p_tmp),v=False)

		if chi2_tmp < chi2:
			chi2 = chi2_tmp
			#chi2_list.append(chi2)
			p0 = p_tmp
			#p0_list.append(p0)
		else:
			count += 1

		if returnchain == True:
			chi2_list.append(chi2_tmp)
			p0_list.append(p_tmp)

	if returnchain:
		return p0, np.array(p0_list), np.array(chi2_list)
	else:
		return p0

def dir(dirname):
	if os.path.exists(dirname) == False:
		os.mkdir(dirname)
	return dirname

def sort2(x,y):
	"""
	from Troxel's destest
	Sorts and matches two arrays of unique object ids (in DES this is coadd_objects_id).
	"""

	u_idx_x = np.argsort(x)
	u_idx_y = np.argsort(y)
	i_xy = np.intersect1d(x, y, assume_unique=True)
	i_idx_x = u_idx_x[x[u_idx_x].searchsorted(i_xy)]
	i_idx_y = u_idx_y[y[u_idx_y].searchsorted(i_xy)]

	return i_idx_x, i_idx_y


def cosmolike_w_cov(data, index):
	#locates the covariance values if indecies in index given a cosmolike file data (columns)
	val = data[-1] + data[-2]
	ix = data[0]
	iy = data[1]
	theta = np.sort(np.unique(data[2]))*180.*60./np.pi

	s = np.in1d(ix,index)
	ix = ix[s]
	iy = iy[s]
	val = val[s]
	s = np.in1d(iy,index)
	ix = ix[s]
	iy = iy[s]
	val = val[s]

	covmat = np.zeros((len(index), len(index)))
	for i, ii in enumerate(index):
		for j, jj in enumerate(index):
			v = val[(ix == ii)*(iy == jj)]
			if len(v) != 1:
				v = [0.]
				print 'no value in ', i, j
			covmat[i,j] = v[0]
			covmat[j,i] = v[0]
	return theta, covmat

def load_cosmolike_cov(filename, twocols=False):
	data = np.loadtxt(filename,unpack=True)

	index = np.unique(data[0])
	covmat = np.zeros((len(index), len(index)))
	for i in xrange(len(data[0])):
		if twocols == True:
			val = data[2][i]+data[3][i]
		else:
			val = data[2][i]
		covmat[int(data[0][i]),int(data[1][i])] =  val

	return covmat

def cutout_w_cov(covmat, lastn=-100):
	return covmat[lastn:,lastn:]

def cov_sym(covmat):
	n = len(covmat)
	assert covmat[0,n-1] != 0.
	for i in xrange(n):
		for j in xrange(n):
			if covmat[i,j] == 0.:
				covmat[i,j] = covmat[j,i]
	return covmat


def get_level(h, cl, tol = 0.1):
	#find level that contains fraction cl of the histogram z
	z = h/np.sum(h)

	level = z.max()/2.
	level_upper = np.array([z.max()])
	level_lower = np.array([0.])
	vol = sum(z[z>level])
	while np.abs(vol-cl) > tol:
		if vol > cl:
			level = (level_upper.min()-level)/2.
			level_lower = np.append(level_lower, level)
		else:
			level = (level-level_lower.max())/2.
			level_upper = np.append(level_upper, level)

		vol = sum(z[z>level])

	return level

class test:
	def __init__(self, data):
		self.data = data
		self.n = len(data[0])
		data1 = np.zeros(data.shape)
		for i in xrange(self.n):
			setattr(self, 'col'+str(i), data[:,i])
	def sort(self):
		order = ['col'+str(i) for i in xrange(self.n)]
		s = np.argsort(self, order=order)
		print s
		print self.data[s]



